import express from 'express';
import bodyParser from 'body-parser';
import DB from './utils/database.js';
import graphqlExpress from 'express-graphql';
import schema from './schemata.js';

DB.connect();

// 使用ポートの設定
const PORT = 8080;

// Express初期化
const app = express();

app.use("/hello", (req, res) => {
    res.send('Hello World!')
});

app.use(
    "/graphql",
    bodyParser.json(),
    graphqlExpress({ schema, graphiql: true })
);

app.listen(PORT, () => {
    console.log(`Started on port ${PORT}`)
});