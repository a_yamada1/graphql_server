import mongoose from 'mongoose';

const DB = {
    connect: () => {
        const host = 'localhost:27017'; // ホスト
        const user = 'root'; // ユーザー
        const password = 'pass'; // パスワード
        const database = 'graphql_database'; // データベース名

        mongoose.connect(`mongodb://${user}:${password}@${host}/${database}?authSource=admin`, { useNewUrlParser: true });
    }
}

export default DB;
